# Computação Concorrente

## Laboratório 6:  Programa de leitura escrita(exclusiva) com prioridade pra escrita

<br>

### Michel Monteiro Schorr 120017379

<br><br>

### Dentro da pasta lab, irá encontrar 3 pastas:  
**execs**, contendo os executaveis  
**src**, contendo o condigo fonte principal

<br><br>

## Comandos

Os comandos para compilar e executar são os seguintes:
<br>
<br>


``` 
gcc ./lab/src/lab6.c -o ./lab/execs/lab6 -Wall -lpthread -lm
./lab/execs/lab6
```
<br><br><br><br>


# Anotações
Ao colocar-se 4 threads de leitura e 8 de escrita, é comum obter no inicio da execuacao o resultado desejado:
<br><br>
Exemplo de bloqueio das threads de leitura novas quando há uma thread de escrita em espera:<br>
```
L[1] quer ler
Leitora 1 esta lendo
L[2] quer ler
Leitora 2 esta lendo
L[4] quer ler
Leitora 4 esta lendo
E[1] quer escrever
E[1] bloqueou
L[3] quer ler
L[3] bloqueou
```
<br>
PS: alterar o numero de leitoras e escritoras no geral tornou mais incomum a ocorrencia da prioridade de escrita.